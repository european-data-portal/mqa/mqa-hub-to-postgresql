package de.fhg.fokus.edp.mqa_hub2postgres;

import de.fhg.fokus.edp.mqa_hub2postgres.database.DatabaseProvider;
import de.fhg.fokus.edp.mqa_hub2postgres.database.DatabaseProviderImpl;
import de.fhg.fokus.edp.mqa_hub2postgres.model.Catalogue;
import de.fhg.fokus.edp.mqa_hub2postgres.model.Dataset;
import de.fhg.fokus.edp.mqa_hub2postgres.model.Distribution;
import de.fhg.fokus.edp.mqa_hub2postgres.model.Violation;
import de.fhg.fokus.edp.mqa_hub2postgres.model.url_check.UrlCheckRequest;
import de.fhg.fokus.edp.mqa_hub2postgres.model.url_check.UrlCheckResponse;
import de.fhg.fokus.edp.mqa_hub2postgres.rdf.RdfUtil;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static de.fhg.fokus.edp.mqa_hub2postgres.ApplicationConfig.*;


public class SparqlFetchVerticle extends AbstractVerticle {

    private static final Logger LOG = LoggerFactory.getLogger(SparqlFetchVerticle.class);

    private WebClient webClient;
    private DatabaseProvider databaseProvider;


    @Override
    public void start(Future<Void> startFuture) {

        WebClientOptions clientOptions = new WebClientOptions()
                .setDefaultHost(config().getString(ENV_HUB_HOST, DEFAULT_HUB_HOST))
                .setDefaultPort(config().getInteger(ENV_HUB_PORT, DEFAULT_HUB_PORT));

        webClient = WebClient.create(vertx, clientOptions);

//        vertx.eventBus().consumer(ADDRESS_FETCH_SPARQL, this::handleFetchRequest);
//        vertx.eventBus().consumer(ADDRESS_CALLBACK, this::handleUrlCheckCallback);

        databaseProvider = new DatabaseProviderImpl(vertx);

        startFuture.complete();
    }

    private void handleFetchRequest(Message<String> message) {

        getCatalogues().setHandler(catalogueHandler -> {
            if (catalogueHandler.succeeded()) {
                catalogueHandler.result().forEach(catalogue ->
                        databaseProvider.upsertCatalogue(catalogue, catalogueUpsertHandler -> {
                            if (catalogueUpsertHandler.succeeded()) {
                                handleDatasets(catalogue.getId(), config().getInteger(ENV_HUB_PAGE_SIZE, DEFAULT_HUB_PAGE_SIZE), 0);
                            }
                        }));
            } else {
                LOG.error("{}", catalogueHandler.cause());
            }
        });
    }

    private void handleUrlCheckCallback(Message<String> message) {

        JsonObject payload = new JsonObject(message.body());

        String distributionId = payload.getString("distributionId");
        List<UrlCheckResponse> responses = Arrays.asList(Json.decodeValue(message.body(), UrlCheckResponse[].class));

        databaseProvider.updateUrlCheckInfo(distributionId, responses, updateHandler -> {
            if (updateHandler.failed()) {
                LOG.error("Failed to update URL check info for distribution with ID [{}] : {}", distributionId, updateHandler.cause());
            }
        });
    }

    private Future<List<Catalogue>> getCatalogues() {
        Future<List<Catalogue>> completionFuture = Future.future();

        webClient.get("/catalogues")
                .expect(ResponsePredicate.SC_OK)
                .send(requestHandler -> {
                    if (requestHandler.succeeded() && ((JsonObject) requestHandler.result()).getBoolean("success")) {
                        Model catalogueModel = ModelFactory.createDefaultModel().read(((JsonObject) requestHandler.result()).getString("content"));
                        completionFuture.complete(RdfUtil.modelToCatalogues(catalogueModel));
                    } else {
                        completionFuture.fail("Failed to fetch catalogues: {}" + requestHandler.cause());
                    }
                });

        return completionFuture;
    }

    private void handleDatasets(long catalogueId, int limit, int offset) {
        webClient.get("/datasets")
                .setQueryParam("catalogue", String.valueOf(catalogueId))
                .setQueryParam("limit", String.valueOf(limit))
                .setQueryParam("offset", String.valueOf(offset))
                .expect(ResponsePredicate.SC_OK)
                .send(requestHandler -> {
                    LOG.info("Attempting to fetch datasets [{} to {}]", offset, limit + offset);

                    if (requestHandler.succeeded() && ((JsonObject) requestHandler.result()).getBoolean("success")) {

                        Model datasetModel = ModelFactory.createDefaultModel().read(((JsonObject) requestHandler.result()).getString("content"));

                        List<Dataset> datasets = RdfUtil.modelToDataset(datasetModel);
                        List<Future> violationFutures = new ArrayList<>();

                        datasets.forEach(dataset -> {

                            List<Distribution> distributions = RdfUtil.modelToDistribution(datasetModel);
                            dataset.setDistributions(distributions);

                            dataset.setLicenceId(dataset.getDistributions().size() > 0
                                    ? dataset.getDistributions().get(0).getLicenceId()
                                    : "");


                            Future<Void> violationFuture = Future.future();
                            violationFutures.add(violationFuture);

                            getViolations(dataset.getInstanceId()).setHandler(violationHandler -> {
                                if (violationHandler.succeeded()) {
                                    dataset.setViolations(violationHandler.result());
                                } else {
                                    dataset.setViolations(new ArrayList<>());
                                    LOG.error("Failed to get violations: {}", violationHandler.cause());
                                }

                                violationFuture.complete();
                            });
                        });

//                        CompositeFuture.all(violationFutures).setHandler(violationHandler ->
//                                databaseProvider.upsertDataset(catalogueId, datasets, upsertHandler -> {
//                                    if (upsertHandler.succeeded()) {
//
//                                        String applicationHostUrl = config().getString(ENV_APPLICATION_HOST, "http://127.0.0.1:8087");
//                                        String urlCheckEndpoint = config().getString(ENV_URL_CHECK_ENDPOINT, "http://127.0.0.1:8120/check");
//
//                                        if (applicationHostUrl != null && !applicationHostUrl.isEmpty()
//                                                && urlCheckEndpoint != null && !urlCheckEndpoint.isEmpty()) {
//
//                                            datasets.forEach(dataset -> {
//                                                if (dataset.getDistributions() != null)
//                                                    dataset.getDistributions().forEach(distribution ->
//                                                            sendUrlCheckRequest(distribution, applicationHostUrl, urlCheckEndpoint));
//                                            });
//                                        } else {
//                                            LOG.warn("Variable(s) for URL check have not been set, skipping URL check");
//                                        }
//                                    } else {
//                                        LOG.error("Failed to upsert datasets: {}", upsertHandler.cause());
//                                    }
//                                }));

                        // recursively fetch more datasets
                        handleDatasets(catalogueId, limit, offset + limit);
                    } else {
                        LOG.error("Failed to fetch datasets: {}", requestHandler.cause());
                    }
                });
    }

    private Future<List<Violation>> getViolations(String datasetId) {
        Future<List<Violation>> completionFuture = Future.future();



        completionFuture.complete();

        return completionFuture;
    }

    private void sendUrlCheckRequest(Distribution distribution, String applicationHostUrl, String urlCheckEndpoint) {

        List<String> urls = new ArrayList<>();
        urls.add(distribution.getAccessUrl());

        if (distribution.getDownloadUrl() != null
                && !distribution.getDownloadUrl().isEmpty()
                && !distribution.getDownloadUrl().equals(distribution.getAccessUrl())) {

            urls.add(distribution.getDownloadUrl());
        }

        String callback = StringUtils.removeEnd(applicationHostUrl, "/")
                + "/urlCallback/" + distribution.getInstanceId();

        UrlCheckRequest request = new UrlCheckRequest();
        request.setUrls(urls);
        request.setCallback(callback);

        webClient.postAbs(urlCheckEndpoint)
                .expect(ResponsePredicate.SC_ACCEPTED)
                .sendJson(request, handler -> {
                    if (handler.failed())
                        LOG.error("Failed to send UrlCheckRequest for distribution [{}]: {}", distribution.getInstanceId(), handler.cause());
                });
    }

    @Override
    public void stop(Future<Void> future) {
        databaseProvider.tearDown();
        future.complete();
    }
}
