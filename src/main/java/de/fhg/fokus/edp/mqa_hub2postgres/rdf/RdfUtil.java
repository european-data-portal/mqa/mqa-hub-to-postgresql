package de.fhg.fokus.edp.mqa_hub2postgres.rdf;

import de.fhg.fokus.edp.mqa_hub2postgres.model.Catalogue;
import de.fhg.fokus.edp.mqa_hub2postgres.model.Dataset;
import de.fhg.fokus.edp.mqa_hub2postgres.model.Distribution;
import de.fhg.fokus.edp.mqa_hub2postgres.rdf.vocabularies.DCATAP;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

public class RdfUtil {

    private static final Logger LOG = LoggerFactory.getLogger(RdfUtil.class);

    private static final String DATASET_BASE_URI = "http://europeandataportal.eu/set/dataset/";

    public static List<Catalogue> modelToCatalogues(Model model) {
        List<Catalogue> result = new ArrayList<>();

        model.listSubjectsWithProperty(RDF.type, DCATAP.DcatCatalog).forEachRemaining(catalogueResource -> {
            Catalogue catalogue = new Catalogue();

            catalogue.setInstanceId(catalogueResource.getURI());

            try {
                catalogue.setTitle(PropertyParser.getDctTitle(catalogueResource));
            } catch (PropertyNotAvailableException e) {
                LOG.debug(e.getMessage());
            }

            // TODO clean title
            catalogue.setName(catalogue.getTitle());

            try {
                catalogue.setSpatial(PropertyParser.getDctSpatial(catalogueResource).getString("title"));
            } catch (PropertyNotAvailableException e) {
                LOG.debug(e.getMessage());
            }

            result.add(catalogue);
        });

        return result;
    }


    public static List<Dataset> modelToDataset(Model model) {

        List<Dataset> result = new ArrayList<>();

        model.listSubjectsWithProperty(RDF.type, DCATAP.DcatDataset).forEachRemaining(datasetResource -> {
            Dataset dataset = new Dataset();

            dataset.setInstanceId(datasetResource.getURI());

            try {
                dataset.setTitle(PropertyParser.getDctTitle(datasetResource));
            } catch (PropertyNotAvailableException e) {
                LOG.debug(e.getMessage());
            }

            // TODO clean title
            dataset.setName(dataset.getTitle());

            result.add(dataset);
        });

        return result;
    }

    public static List<Distribution> modelToDistribution(Model model) {
        List<Distribution> result = new ArrayList<>();

        model.listSubjectsWithProperty(RDF.type, DCATAP.DcatDistribution).forEachRemaining(distributionResource -> {
            Distribution distribution = new Distribution();

            distribution.setInstanceId(distributionResource.getURI());

            try {
                distribution.setAccessUrl(PropertyParser.getDcatAccessUrl(distributionResource));
            } catch (PropertyNotAvailableException e) {
                LOG.debug(e.getMessage());
            }

            try {
                distribution.setFormat(PropertyParser.getDctFormat(distributionResource));
            } catch (PropertyNotAvailableException e) {
                LOG.debug(e.getMessage());
            }

            try {
                distribution.setLicenceId(PropertyParser.getDctLicense(distributionResource).getString("id"));
            } catch (PropertyNotAvailableException e) {
                LOG.debug(e.getMessage());
            }

            try {
                distribution.setDownloadUrl(PropertyParser.getDcatDownloadUrl(distributionResource).getString("TODO"));
            } catch (PropertyNotAvailableException e) {
                LOG.debug(e.getMessage());
            }

            // TODO machine readable

            result.add(distribution);
        });

        return result;
    }

    private static String buildDatasetURI(String id) {
        return DATASET_BASE_URI + generateID(id);
    }

    private static String generateID(String name) {
        //normalize
        name = Normalizer.normalize(name, Normalizer.Form.NFKD);
        //remove all '%'
        //replace non-word-characters with '-'
        //then combine multiple '-' into one
        return name.replaceAll("%", "").replaceAll("\\W", "-").replaceAll("-+", "-").toLowerCase();
    }
}

