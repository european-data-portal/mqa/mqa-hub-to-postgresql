package de.fhg.fokus.edp.mqa_hub2postgres.rdf;

public class PropertyNotAvailableException extends Exception {

    public PropertyNotAvailableException() {
        super();
    }

    public PropertyNotAvailableException(String message) {
        super(message);
    }

}
