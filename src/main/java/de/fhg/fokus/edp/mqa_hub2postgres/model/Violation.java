package de.fhg.fokus.edp.mqa_hub2postgres.model;

import java.time.LocalDateTime;

public class Violation {

    private long id;
    private LocalDateTime dbUpdate;
    private String instance;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getDbUpdate() {
        return dbUpdate;
    }

    public void setDbUpdate(LocalDateTime dbUpdate) {
        this.dbUpdate = dbUpdate;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Violation{" +
                "id=" + id +
                ", dbUpdate=" + dbUpdate +
                ", instance='" + instance + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
