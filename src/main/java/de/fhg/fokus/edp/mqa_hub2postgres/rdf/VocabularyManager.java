package de.fhg.fokus.edp.mqa_hub2postgres.rdf;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;
import io.vertx.core.json.JsonObject;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;

public class VocabularyManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(VocabularyManager.class);

    public static Model model = ModelFactory.createDefaultModel();

    public static void init(JsonObject config, Vertx vertx, Handler<AsyncResult<Void>> handler) {
        if (config.getBoolean("HUB_LOAD_VOCABULARY", true)) {
            vertx.<Void>executeBlocking(fut -> loadVocabularies(vertx, fut.completer()), res -> {
                if (res.succeeded()) {
                    handler.handle(Future.succeededFuture());
                } else {
                    handler.handle(Future.failedFuture(res.cause()));
                }
            });
        } else {
            handler.handle(Future.succeededFuture());
        }
    }

    private static void loadVocabularies(Vertx vertx, Handler<AsyncResult<Void>> handler) {
        loadVocabulary(vertx, "vocabularies/languages-skos.rdf");
        loadVocabulary(vertx, "vocabularies/data-theme-skos-ap-act.rdf");
        loadVocabulary(vertx, "vocabularies/filetypes-skos.rdf");
        loadVocabulary(vertx, "vocabularies/countries-skos-ap-act.rdf");
        loadVocabulary(vertx, "vocabularies/licences-skos-edp.rdf");
        handler.handle(Future.succeededFuture());
    }

    private static void loadVocabulary(Vertx vertx, String file) {
        LOGGER.info("Init " + file);
        FileSystem fileSystem = vertx.fileSystem();
        Buffer buffer = fileSystem.readFileBlocking(file);
        RDFDataMgr.read(model, new ByteArrayInputStream(buffer.getBytes()), Lang.RDFXML);
        LOGGER.info("Created " + file + " Model");
    }
}
