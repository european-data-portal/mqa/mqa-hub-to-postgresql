package de.fhg.fokus.edp.mqa_hub2postgres.rdf;

import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import org.apache.jena.rdf.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.Normalizer;

public class PropertyHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyHelper.class);

    
    static String getSingleLiteral(Resource resource, Property property) throws PropertyNotAvailableException {
        String result;
        try {
            if(resource.getProperty(property) == null) {
                throw new PropertyNotAvailableException(property.toString() + " is not set");
            }
            if(!resource.getProperty(property).getObject().isLiteral()) {
                throw new PropertyNotAvailableException(property.toString() + " is not a Literal");
            }
            result = resource.getProperty(property).getLiteral().getString();
        } catch (RuntimeException e) {
            throw new PropertyNotAvailableException(e.getMessage());
        }
        return result;
    }


    static JsonObject getMultilingualLiteral(Resource resource, Property property) throws PropertyNotAvailableException {
        JsonObject result = new JsonObject();

        try {
            StmtIterator stmtIterator = resource.listProperties(property);
            if(!stmtIterator.hasNext()) {
                throw new PropertyNotAvailableException(property.toString() + " not set.");
            }
            while(stmtIterator.hasNext()) {
                Statement stmt = stmtIterator.nextStatement();
                if(!stmt.getObject().isLiteral()) {
                    LOGGER.warn("Found value for " + property.toString() + ", which is not a literal.");
                } else {
                    Literal literal = stmt.getLiteral();
                    String lang = literal.getLanguage();
                    if(!lang.isEmpty()) {
                        result.put(lang, literal.getString());
                    } else {
                        result.put("en", literal.getString());
                    }
                }
            }
        } catch (RuntimeException e) {
            throw new PropertyNotAvailableException(e.getMessage());
        }
        if(result.isEmpty()) {
            throw new PropertyNotAvailableException(property.toString() + " not set.");
        }

        return result;
    }

    static String getEnglishLiteral(Resource resource, Property property) throws PropertyNotAvailableException {
        try {
            StmtIterator stmtIterator = resource.listProperties(property);
            if(!stmtIterator.hasNext()) {
                throw new PropertyNotAvailableException(property.toString() + " not set.");
            }
            while(stmtIterator.hasNext()) {
                Statement stmt = stmtIterator.nextStatement();
                if(!stmt.getObject().isLiteral()) {
                    LOGGER.warn("Found value for " + property.toString() + ", which is not a literal.");
                } else {
                    Literal literal = stmt.getLiteral();
                    String lang = literal.getLanguage();
                    if(lang.equals("en")) {
                        return literal.getString();
                    }
                }
            }
        } catch (RuntimeException e) {
            throw new PropertyNotAvailableException(e.getMessage());
        }
        return null;
    }


    static void getProperty(Resource res, Property prop, Handler<Statement> handler) {
        if(res.getProperty(prop) != null    ) {
            handler.handle(res.getProperty(prop));
        }
    }

    static Resource getPropertyAsResource(Resource resource, Property property) throws PropertyNotAvailableException  {
        Statement stmt = resource.getProperty(property);
        if(stmt != null) {
            if(stmt.getObject().isResource()) {
                return stmt.getResource();
            } else {
                throw new PropertyNotAvailableException(property.toString() + " is not a resource");
            }
        } else {
            throw new PropertyNotAvailableException(property.toString() + " is not defined");
        }
    }

    static Resource getStatementAsResource(Statement statement) throws PropertyNotAvailableException  {
        if(statement.getObject().isResource()) {
            return statement.getResource();
        } else {
            throw new PropertyNotAvailableException(statement.toString() + " is not a resource");
        }
    }

    static StmtIterator getPropertiesAsStmtIterator(Resource resource, Property property) throws PropertyNotAvailableException  {
        StmtIterator stmtIterator = resource.listProperties(property);
        if(stmtIterator.hasNext()) {
            return stmtIterator;
        } else {
            throw new PropertyNotAvailableException(property.toString() + " is not set");
        }
    }

    static Resource getResourceFromVocabulary(Resource resource) {
        Model model = VocabularyManager.model;
        if(model.contains(resource, null, (RDFNode) null)) {
            return model.getResource(resource.getURI());
        } else {
            return null;
        }
    }

    static String generateID(String name) {
        //normalize
        name = Normalizer.normalize(name, Normalizer.Form.NFKD);
        //remove all '%'
        //replace non-word-characters with '-'
        //then combine multiple '-' into one
        return name.replaceAll("%", "").replaceAll("\\W", "-").replaceAll("-+", "-").toLowerCase();
    }

    static String extractLabelFromURI(String uri){
        String label = uri.substring(uri.lastIndexOf("/") + 1).trim();
        return label;
    }

}
