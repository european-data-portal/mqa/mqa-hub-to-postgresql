package de.fhg.fokus.edp.mqa_hub2postgres.database;

import de.fhg.fokus.edp.mqa_hub2postgres.model.Catalogue;
import de.fhg.fokus.edp.mqa_hub2postgres.model.Dataset;
import de.fhg.fokus.edp.mqa_hub2postgres.model.Distribution;
import de.fhg.fokus.edp.mqa_hub2postgres.model.Violation;
import de.fhg.fokus.edp.mqa_hub2postgres.model.url_check.UrlCheckResponse;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static de.fhg.fokus.edp.mqa_hub2postgres.ApplicationConfig.*;
import static de.fhg.fokus.edp.mqa_hub2postgres.database.PostgresQueries.*;

public class DatabaseProviderImpl implements DatabaseProvider {

    private static final Logger LOG = LoggerFactory.getLogger(DatabaseProvider.class);

    private JDBCClient dbClient;

    public DatabaseProviderImpl(Vertx vertx) {
        JsonObject env = vertx.getOrCreateContext().config();

        JsonObject config = new JsonObject()
                .put("url", env.getString(ENV_PGSQL_SERVER_HOST, DEFAULT_PGSQL_SERVER_HOST))
                .put("driver_class", "org.postgresql.Driver")
                .put("user", env.getString(ENV_PGSQL_USERNAME, DEFAULT_PGSQL_USERNAME))
                .put("password", env.getString(ENV_PGSQL_PASSWORD, DEFAULT_PGSQL_PASSWORD))
                .put("max_pool_size", 30);

        LOG.debug("Postgres config: {}", config);

        dbClient = JDBCClient.createShared(vertx, config);

        // FIXME this needs to be done in a blocking manner
//        // test if DB connection can be established
//        Future<Boolean> dbConnectionFuture = Future.future();
//        dbClient.getConnection(res ->
//            dbConnectionFuture.complete(res.succeeded()));
//
//        if (dbConnectionFuture.result())
//            throw new SQLException("Could not acquire database connection");
    }

    @Override
    public void tearDown() {
        dbClient.close();
    }

    @Override
    public void upsertCatalogue(Catalogue catalogue, Handler<AsyncResult<Long>> resultHandler) {
        dbClient.updateWithParams(UPSERT_CATALOGUES, catalogueToSqlParams(catalogue), catalogueUpsertHandler -> {
            if (catalogueUpsertHandler.succeeded()) {
                Long catalogueId = catalogueUpsertHandler.result().getKeys().getLong(0);
                resultHandler.handle(Future.succeededFuture(catalogueId));
            } else {
                resultHandler.handle(Future.failedFuture("Failed to upsert catalogues: " + catalogueUpsertHandler.cause()));
            }
        });
    }

    @Override
    public void upsertDataset(Long catalogueId, Dataset dataset, Handler<AsyncResult<Void>> resultHandler) {
        Future<Dataset> upsertDatasetFuture = Future.future();

        dbClient.updateWithParams(UPSERT_DATASETS, datasetToSqlParams(dataset, catalogueId), datasetUpsertHandler -> {
            if (datasetUpsertHandler.succeeded()) {
                dataset.setId(datasetUpsertHandler.result().getKeys().getLong(0));
                LOG.debug("Upserted dataset with ID [{}]", dataset.getId());
                upsertDatasetFuture.complete(dataset);
            } else {
                LOG.error("Failed to upsert dataset {}: {}", dataset.getId(), datasetToSqlParams(dataset, catalogueId), datasetUpsertHandler.cause());
                upsertDatasetFuture.fail(datasetUpsertHandler.cause());
            }
        });

        upsertDatasetFuture.compose(upsertedDataset -> {

            Future<Dataset> deleteDistributionsFuture = Future.future();

            dbClient.updateWithParams(DELETE_DISTRIBUTIONS, new JsonArray().add(upsertedDataset.getId()), deletionHandler -> {
                if (deletionHandler.succeeded()) {
                    LOG.debug("Deleted distributions for dataset with ID [{}]", upsertedDataset.getId());
                    deleteDistributionsFuture.complete(upsertedDataset);
                } else {
                    LOG.error("Failed to delete distributions for dataset with ID [{}] : {}", upsertedDataset.getId(), deletionHandler.cause());
                    deleteDistributionsFuture.fail("Failed to delete distributions for dataset with ID [" + upsertedDataset.getId() + "] : " + deletionHandler.cause());
                }
            });

            return deleteDistributionsFuture;

        }).compose(upsertedDataset -> {

            Future<Dataset> deleteViolationsFuture = Future.future();

            dbClient.updateWithParams(DELETE_VIOLATIONS, new JsonArray().add(upsertedDataset.getId()), deletionHandler -> {
                if (deletionHandler.succeeded()) {
                    LOG.debug("Deleted violations for dataset with ID [{}]", upsertedDataset.getId());
                    deleteViolationsFuture.complete(upsertedDataset);
                } else {
                    LOG.error("Failed to delete violations for dataset with ID [{}] : {}", upsertedDataset.getId(), deletionHandler.cause());
                    deleteViolationsFuture.fail("Failed to delete violations for dataset with ID [" + upsertedDataset.getId() + "] : " + deletionHandler.cause());
                }
            });

            return deleteViolationsFuture;

        }).compose(upsertedDataset -> {

            Future<Dataset> insertDistributionsFuture = Future.future();

            if (dataset.getDistributions() != null && !dataset.getDistributions().isEmpty()) {
                dbClient.getConnection(connectionHandler -> {
                    if (connectionHandler.succeeded()) {
                        connectionHandler.result().batchWithParams(INSERT_DISTRIBUTIONS, batchDistributions(upsertedDataset), batchHandler -> {
                            if (batchHandler.succeeded()) {
                                LOG.debug("Inserted [{}] distributions for catalogue with ID [{}]", upsertedDataset.getDistributions().size(), catalogueId);
                                insertDistributionsFuture.complete(upsertedDataset);
                            } else {
                                LOG.error("Failed to insert [{}] distributions for catalogue with ID [{}]: {}", upsertedDataset.getDistributions().size(), catalogueId, batchHandler.cause());
                                insertDistributionsFuture.fail("Failed to insert distributions for catalogue with ID " + catalogueId);
                            }

                            connectionHandler.result().close();
                        });
                    } else {
                        LOG.error("Failed to acquire JDBC connection: {}", connectionHandler.cause().getMessage());
                        insertDistributionsFuture.fail("Failed to acquire JDBC connection: " + connectionHandler.cause());
                    }
                });
            } else {
                insertDistributionsFuture.complete(dataset);
            }

            return insertDistributionsFuture;

        }).compose(upsertedDataset -> {

            Future<Void> insertViolationsFuture = Future.future();

            if (dataset.getViolations() != null && !dataset.getViolations().isEmpty()) {
                dbClient.getConnection(connectionHandler -> {
                    if (connectionHandler.succeeded()) {
                        connectionHandler.result().batchWithParams(INSERT_VIOLATIONS, batchViolations(upsertedDataset), batchHandler -> {
                            if (batchHandler.succeeded()) {
                                LOG.debug("Inserted [{}] violations for catalogue with ID [{}]", upsertedDataset.getViolations().size(), catalogueId);
                                insertViolationsFuture.complete();
                            } else {
                                LOG.error("Failed to insert [{}] violations for catalogue with ID [{}]: {}", upsertedDataset.getViolations().size(), catalogueId, batchHandler.cause());
                                insertViolationsFuture.fail("Failed to insert [" + upsertedDataset.getViolations().size() + "] violations: " + batchHandler.cause());
                            }

                            connectionHandler.result().close();
                        });
                    } else {
                        LOG.error("Failed to acquire JDBC connection: {}", connectionHandler.cause().getMessage());
                        insertViolationsFuture.fail("Failed to acquire JDBC connection: " + connectionHandler.cause());
                    }
                });
            } else {
                insertViolationsFuture.complete();
            }

            return insertViolationsFuture;

        }).setHandler(processDataset -> {
            if (processDataset.succeeded()) {
                LOG.debug("Successfully processed dataset [{}]", dataset.toString());
                resultHandler.handle(Future.succeededFuture());
            } else {
                resultHandler.handle(Future.failedFuture("Failed to process " + dataset.getId() + ": " + processDataset.cause()));
            }
        });
    }

    @Override
    public void updateUrlCheckInfo(String distributionId, List<UrlCheckResponse> responses, Handler<AsyncResult<Void>> resultHandler) {

        dbClient.queryWithParams(GET_DISTRIBUTION, new JsonArray().add(distributionId), getDistributionHandler -> {
            if (getDistributionHandler.succeeded()) {
                if (getDistributionHandler.result().getRows().size() == 1) {

                    String accessUrl = getDistributionHandler.result().getRows().get(0).getString("access_url");
                    Integer statusAccessUrl = getDistributionHandler.result().getRows().get(0).getInteger("status_access_url");

                    UrlCheckResponse accessResponse = responses.stream()
                            .filter(res -> res.getUrl().equals(accessUrl))
                            .findAny()
                            .orElse(null);


                    String downloadUrl = getDistributionHandler.result().getRows().get(0).getString("download_url");
                    Integer statusDownloadUrl = getDistributionHandler.result().getRows().get(0).getInteger("status_download_url");

                    UrlCheckResponse downloadResponse = responses.stream()
                            .filter(res -> res.getUrl().equals(downloadUrl))
                            .findAny()
                            .orElse(null);


                    if (accessResponse != null) {
                        if (statusAccessUrl != accessResponse.getStatusCode()) {
                            if (downloadResponse == null || statusDownloadUrl == downloadResponse.getStatusCode()) {
                                dbClient.queryWithParams(UPDATE_URL_CHECK_INFO, urlCheckToSqlParams(distributionId, accessResponse), distributionUpdateHandler -> {
                                    if (distributionUpdateHandler.succeeded()) {
                                        resultHandler.handle(Future.succeededFuture());
                                    } else {
                                        resultHandler.handle(Future.failedFuture("Failed to update distribution with ID [" + distributionId + "]"));
                                    }
                                });
                            } else {
                                dbClient.queryWithParams(UPDATE_URL_CHECK_INFO_WITH_DOWNLOAD_URL, urlCheckToSqlParams(distributionId, accessResponse, downloadResponse), distributionUpdateHandler -> {
                                    if (distributionUpdateHandler.succeeded()) {
                                        resultHandler.handle(Future.succeededFuture());
                                    } else {
                                        resultHandler.handle(Future.failedFuture("Failed to update distribution with ID [" + distributionId + "]"));
                                    }
                                });
                            }
                        }
                    } else {
                        LOG.error("URL check response did not contain access URL [" + accessUrl + "] for distribution with ID [" + distributionId + "]");
                        resultHandler.handle(Future.failedFuture("URL check response did not contain access URL [" + accessUrl + "] for distribution with ID [" + distributionId + "]"));
                    }
                } else if (getDistributionHandler.result().getRows().size() == 0) {
                    LOG.error("Distribution with ID [" + distributionId + "] not found");
                    resultHandler.handle(Future.failedFuture("Distribution with ID [" + distributionId + "] not found"));
                } else {
                    LOG.error("Multiple distributions with ID [" + distributionId + "] found");
                    resultHandler.handle(Future.failedFuture("Multiple distributions with ID [" + distributionId + "] found"));
                }
            } else {
//                resultHandler.handle(Future.failedFuture(getDistributionHandler.cause()));
                LOG.error("Failed to fetch distributions: " + getDistributionHandler.cause());
                resultHandler.handle(Future.failedFuture("Failed to fetch distributions: " + getDistributionHandler.cause()));
            }
        });
    }

    @Override
    public void upsertKnownLicences(List<String> licences, Handler<AsyncResult<Void>> resultHandler) {
        dbClient.query(DELETE_LICENCES, deleteHandler -> {
            if (deleteHandler.succeeded()) {

                List<JsonArray> batchParams = licences.stream()
                        .map(licence ->
                                new JsonArray().add(LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond()).add(licence))
                        .collect(Collectors.toList());

                dbClient.getConnection(connectionHandler -> {
                    if (connectionHandler.succeeded()) {
                        connectionHandler.result().batchWithParams(INSERT_LICENCES, batchParams, batchHandler -> {
                            if (batchHandler.succeeded()) {
                                LOG.info("Inserted [{}] licences", licences.size());
                                resultHandler.handle(Future.succeededFuture());
                            } else {
                                LOG.error("Failed to insert [{}] licences: {}", licences.size(), batchHandler.cause());
                                resultHandler.handle(Future.failedFuture(batchHandler.cause()));
                            }

                            connectionHandler.result().close();
                        });
                    } else {
                        LOG.error("Failed to acquire JDBC connection: {}", connectionHandler.cause().getMessage());
                        resultHandler.handle(Future.failedFuture("Failed to acquire JDBC connection: " + connectionHandler.cause()));
                    }
                });
            } else {
                LOG.error("Failed to delete licences: {}", deleteHandler.cause().getMessage());
                resultHandler.handle(Future.failedFuture(deleteHandler.cause()));
            }
        });
    }

    @Override
    public void deleteOutdatedEntities(String date, Handler<AsyncResult<Void>> resultHandler) {

        Future<Void> deleteViolationsFuture = Future.future();
        deleteEntity(DELETE_VIOLATIONS_WITH_TIMESTAMP, date, deleteViolationsFuture);

        deleteViolationsFuture.compose(deleteViolationsResult -> {
            Future<Void> deleteDistributionsFuture = Future.future();
            deleteEntity(DELETE_DISTRIBUTIONS_WITH_TIMESTAMP, date, deleteDistributionsFuture);
            return deleteDistributionsFuture;

        }).compose(deleteDistributionsResult -> {
            Future<Void> deleteDatasetsFuture = Future.future();
            deleteEntity(DELETE_DATASETS_WITH_TIMESTAMP, date, deleteDatasetsFuture);
            return deleteDatasetsFuture;

        }).compose(deleteDatasetsResult -> {
            Future<Void> deleteCataloguesFuture = Future.future();
            deleteEntity(DELETE_CATALOGUES_WITH_TIMESTAMP, date, deleteCataloguesFuture);
            return deleteCataloguesFuture;

        }).setHandler(deletionHandler -> {
           if (deletionHandler.succeeded()) {
               resultHandler.handle(Future.succeededFuture());
           } else {
               resultHandler.handle(Future.failedFuture(deletionHandler.cause()));
           }
        });
    }

    private void deleteEntity(String query, String date, Future<Void> future) {
        dbClient.queryWithParams(DELETE_VIOLATIONS_WITH_TIMESTAMP, new JsonArray().add(date), deletionHandler -> {
            if (deletionHandler.succeeded()) {
                future.handle(Future.succeededFuture());
            } else {
                future.handle(Future.failedFuture(deletionHandler.cause()));
            }
        });
    }

    private JsonArray catalogueToSqlParams(Catalogue catalogue) {
        JsonArray result = new JsonArray()
                .add(LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond());

        addSave(catalogue.getInstanceId(), result);
        addSave(catalogue.getName(), result);

        // title has non-null constraint in db
        result.add(catalogue.getTitle() != null
                ? catalogue.getTitle()
                : catalogue.getName());

        addSave(catalogue.getSpatial(), result);
        addSave(catalogue.getDcat(), result);

        return result;
    }

    private JsonArray datasetToSqlParams(Dataset dataset, long catalogueId) {
        JsonArray result = new JsonArray()
                .add(LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond());

        addSave(dataset.getInstanceId(), result);
        addSave(dataset.getLicenceId(), result);
        addSave(dataset.getMachineReadable(), result);
        addSave(dataset.getName(), result);
        addSave(dataset.getTitle(), result);
        addSave(catalogueId, result);

        return result;
    }

    private JsonArray urlCheckToSqlParams(String distributionId, UrlCheckResponse accessUrlCheckResponse) {
        JsonArray result = new JsonArray();

        addSave(accessUrlCheckResponse.getStatusCode(), result);
        addSave(accessUrlCheckResponse.getMessage(), result);

        result.add(LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond());
        result.add(distributionId);

        return result;
    }

    private JsonArray urlCheckToSqlParams(String distributionId, UrlCheckResponse accessUrlCheckResponse, UrlCheckResponse downloadUrlCheckResponse) {
        JsonArray result = new JsonArray();

        addSave(accessUrlCheckResponse.getStatusCode(), result);
        addSave(accessUrlCheckResponse.getMessage(), result);
        result.add(LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond());

        addSave(downloadUrlCheckResponse.getStatusCode(), result);
        addSave(downloadUrlCheckResponse.getMessage(), result);
        result.add(LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond());
        result.add(distributionId);

        return result;
    }

    private List<JsonArray> batchDistributions(Dataset dataset) {
        List<JsonArray> result = new ArrayList<>();

        dataset.getDistributions().forEach(distribution -> {
            JsonArray distParams = new JsonArray();

            addSave(distribution.getAccessErrorMessage(), distParams);
            addSave(distribution.getAccessUrl(), distParams);

            distParams.add(LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond());

            addSave(distribution.getDownloadErrorMessage(), distParams);
            addSave(distribution.getDownloadUrl(), distParams);
            addSave(distribution.getFormat(), distParams);
            addSave(distribution.getMachineReadable(), distParams);
            addSave(distribution.getInstanceId(), distParams);
            addSave(distribution.getStatusAccessUrl(), distParams);
            addSave(distribution.getStatusDownloadUrl(), distParams);

            distParams.add(dataset.getId());

            result.add(distParams);
        });

        return result;
    }

    private List<JsonArray> batchViolations(Dataset dataset) {
        List<JsonArray> result = new ArrayList<>();

        dataset.getViolations().forEach(violation -> {
            JsonArray violationParams = new JsonArray();
            violationParams.add(LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond());

            addSave(violation.getInstance(), violationParams);
            addSave(violation.getName(), violationParams);

            violationParams.add(dataset.getId());

            result.add(violationParams);
        });

        return result;
    }

    private void addSave(Object object, JsonArray jsonArray) {
        if (object != null) {
            jsonArray.add(object);
        } else {
            jsonArray.addNull();
        }
    }
}
