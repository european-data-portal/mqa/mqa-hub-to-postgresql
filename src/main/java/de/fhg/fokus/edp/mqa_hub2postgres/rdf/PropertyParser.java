package de.fhg.fokus.edp.mqa_hub2postgres.rdf;

import de.fhg.fokus.edp.mqa_hub2postgres.rdf.vocabularies.DCATAP;
import de.fhg.fokus.edp.mqa_hub2postgres.rdf.vocabularies.EDP;
import de.fhg.fokus.edp.mqa_hub2postgres.rdf.vocabularies.EUVOC;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Parser for retrieving DCAT-AP properties for humans and plain machines
 * For details refer to DCAT-AP specs version 1.2
 */
public class PropertyParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyParser.class);

    private String baseMessage = "Could not extract ";

    /**
     * dct:description
     * Range: rdfs:Literal
     * Used in: Dataset, Distribution, Catalogue
     *
     * @param resource
     * @return
     */
    static JsonObject getDctDescription(Resource resource) throws PropertyNotAvailableException {
        JsonObject result = PropertyHelper.getMultilingualLiteral(resource, DCATAP.dctDescription);
        return result;
    }

    /**
     * dct:title
     * Range: rdfs:Literal
     * Used in: Dataset, Distribution, Catalogue
     *
     * @param resource
     * @return
     */
    static String getDctTitle(Resource resource) throws PropertyNotAvailableException {
        return PropertyHelper.getSingleLiteral(resource, DCATAP.dctTitle);
    }

    /**
     * dcat:contactPoint
     * Range: vcard:Kind
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonArray getDcatContactPoint(Resource resource) throws PropertyNotAvailableException {
        JsonArray result = new JsonArray();
        StmtIterator stmtIterator = PropertyHelper.getPropertiesAsStmtIterator(resource, DCATAP.dcatContactPoint);

        while(stmtIterator.hasNext()) {
            JsonObject contactPointResult = new JsonObject();
            Statement stmn = stmtIterator.nextStatement();
            Resource contactPoint = PropertyHelper.getStatementAsResource(stmn);

            try {
                Resource type = PropertyHelper.getPropertyAsResource(contactPoint, DCATAP.rdfType);
                contactPointResult.put("type", type.getLocalName());
            } catch (PropertyNotAvailableException e) {
                LOGGER.warn(e.getMessage());
            }

            try {
                String name = PropertyHelper.getSingleLiteral(contactPoint, DCATAP.vcardFn);
                contactPointResult.put("name", name);
            } catch (PropertyNotAvailableException e) {
                LOGGER.warn(e.getMessage());
            }

            try {
                Resource email = PropertyHelper.getPropertyAsResource(contactPoint, DCATAP.vcardHasEmail);
                contactPointResult.put("email", email.getURI());
            } catch (PropertyNotAvailableException e) {
                LOGGER.warn(e.getMessage());
            }

            if(!contactPointResult.isEmpty()) {
                result.add(contactPointResult);
            }

        }
        if(result.isEmpty()) {
            throw new PropertyNotAvailableException(DCATAP.dcatContactPoint.toString() + " is not properly set");
        }
        return result;
    }

    /**
     * dcat:distribution
     * Range: dcat:Distribution
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonArray getDcatDistribution(Resource resource) throws PropertyNotAvailableException {
        JsonArray result = new JsonArray();
        StmtIterator stmtIterator = PropertyHelper.getPropertiesAsStmtIterator(resource, DCATAP.dcatDistribution);
        while(stmtIterator.hasNext()) {
            JsonObject distroResult = new JsonObject();
            Statement stmn = stmtIterator.nextStatement();
            Resource distribution = PropertyHelper.getStatementAsResource(stmn);

            try {
                distroResult.put("title", getDctTitle(distribution));
            } catch (PropertyNotAvailableException e) {
                LOGGER.warn(e.getMessage());
            }

            try {
                distroResult.put("description", getDctDescription(distribution));
            } catch (PropertyNotAvailableException e) {
                LOGGER.warn(e.getMessage());
            }

            try {
                distroResult.put("format", getDctFormat(distribution));
            } catch (PropertyNotAvailableException e) {
                LOGGER.warn(e.getMessage());
            }

            try {
                distroResult.put("access_url", getDcatAccessUrl(distribution));
            } catch (PropertyNotAvailableException e) {
                LOGGER.warn(e.getMessage());
            }

            try {
                distroResult.put("licence", getDctLicense(distribution));
            } catch (PropertyNotAvailableException e) {
                LOGGER.warn(e.getMessage());
            }

            if(!distroResult.isEmpty()) {
                result.add(distroResult);
            }
        }

        if(result.isEmpty()) {
            throw new PropertyNotAvailableException(DCATAP.dcatDistribution.toString() + " is not properly set");
        }

        return result;
    }


    /**
     * dcat:keyword
     * Range: rdfs:Literal
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getDcatKeyword(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:publisher
     * Range: foaf:Agent
     * Used in: Dataset, Catalogue
     *
     * @param resource
     * @return
     */
    static JsonObject getDctPublisher(Resource resource) throws PropertyNotAvailableException {
        JsonObject result = new JsonObject();
        Resource publisher = PropertyHelper.getPropertyAsResource(resource, DCATAP.dctPublisher);

        try {
            Resource type = PropertyHelper.getPropertyAsResource(publisher, DCATAP.rdfType);
            result.put("type", type.getLocalName());
        } catch (PropertyNotAvailableException e) {
            LOGGER.warn(e.getMessage());
        }

        try {
            String name = PropertyHelper.getSingleLiteral(publisher, DCATAP.foafName);
            result.put("name", name);
        } catch (PropertyNotAvailableException e) {
            LOGGER.warn(e.getMessage());
        }

        //ToDo Change search service to homepage here
        try {
            Resource homepage = PropertyHelper.getPropertyAsResource(publisher, DCATAP.foafHomepage);
            result.put("email", homepage.getURI());
        } catch (PropertyNotAvailableException e) {
            LOGGER.warn(e.getMessage());
        }

        if(result.isEmpty()) {
            throw new PropertyNotAvailableException(DCATAP.dctPublisher.toString() + " is not properly set");
        }
        return result;
    }

    /**
     * dcat:theme
     * Range: skos:Concept
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonArray getDcatTheme(Resource resource) throws PropertyNotAvailableException {
        JsonArray result = new JsonArray();
        StmtIterator stmtIterator = resource.listProperties(DCATAP.dcatTheme);
        if(!stmtIterator.hasNext()) {
            throw new PropertyNotAvailableException(DCATAP.dcatTheme.toString() + " not set");
        }
        while(stmtIterator.hasNext()){
            JsonObject themeResult = new JsonObject();
            Statement stmt = stmtIterator.nextStatement();
            if(stmt.getObject().isResource()) {
                Resource res = PropertyHelper.getResourceFromVocabulary(stmt.getResource());
                String ident = PropertyHelper.getSingleLiteral(res, EUVOC.dcIdentifier);
                themeResult.put("id", ident);
                JsonObject label = PropertyHelper.getMultilingualLiteral(res, EUVOC.skosPrefLabel);
                if(label.getString("en") != null) {
                    themeResult.put("title", label.getString("en"));
                } else {
                    themeResult.put("title", "unknown");
                }
                themeResult.put("resource", res.getURI());
            }
            result.add(themeResult);
        }

        if(result.isEmpty()) {
            throw new PropertyNotAvailableException(DCATAP.dcatTheme.toString() + " is not properly set");
        }
        return result;
    }

    /**
     * dct:accessRights
     * Range: dct:RightsStatement
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getDctAccssRights(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:conformsTo
     * Range: dct:Standard
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getDctConformsTo(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * foaf:Page
     * Range: foaf:Document
     * Used in: Dataset, Distribution
     *
     * @param resource
     * @return
     */
    static JsonObject getFoafPage(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:accrualPeriodicity
     * Range: dct:Frequency
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getDctAccrualPeriodicity(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:hasVersion
     * Range: dcat:Dataset
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getDctHasVersion(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:identifier
     * Range: rdfs:Literal
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getDctIdentifier(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:isVersionOf
     * Range: dcat:Dataset
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getDctIsVersionOf(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dcat:LandingPage
     * Range: foaf:Document
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getDcatLandingPage(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:Language
     * Range: dct:LinguisticSystem
     * Used in: Dataset, Distribution, Catalogue
     *
     * @param resource
     * @return
     */
    static JsonArray getDctLanguage(Resource resource) throws PropertyNotAvailableException {
        JsonArray result = new JsonArray();
        try {
            StmtIterator stmtIterator = resource.listProperties(DCATAP.dctLanguage);
            if(!stmtIterator.hasNext()) {
                throw new PropertyNotAvailableException(DCATAP.dctLanguage.toString() + " not set");
            }
            while(stmtIterator.hasNext()){
                Statement stmt = stmtIterator.nextStatement();
                if(stmt.getObject().isResource()) {
                    Resource res = PropertyHelper.getResourceFromVocabulary(stmt.getResource());
                    StmtIterator stms = res.listProperties(EUVOC.atOpMappedCode);
                    while (stms.hasNext()) {
                        RDFNode rdfNode = stms.nextStatement().getObject();
                        String h = rdfNode.asResource().getProperty(EUVOC.dcSource).getString();
                        if(h.equals("iso-639-1")) {
                            result.add(rdfNode.asResource().getProperty(EUVOC.atLegacyCode).getString());
                        }
                    }
                }
            }
        } catch (RuntimeException e) {
            throw new PropertyNotAvailableException(e.getMessage());
        }
        if(result.isEmpty()) {
            throw new PropertyNotAvailableException("No valid " + DCATAP.dctLanguage.toString() + " is set");
        }
        return result;
    }

    /**
     * adms:identifier
     * Range: adms:Identifier
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getAdmsIdentifier(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:provenance
     * Range: dct:ProvenanceStatement
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getDctProvenance(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:relation
     * Range: rdfs:Resource
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getDctRelation(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:issued
     * Range: rdfs:Literal (xsd:date, xsd:dateTime
     * Used in: Dataset, Distribution, Catalgue
     *
     * @param resource
     * @return
     */
    static JsonObject getDctIssued(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:modified
     * Range: rdfs:Literal (xsd:date, xsd:dateTime
     * Used in: Dataset, Distribution, Catalogue
     *
     * @param resource
     * @return
     */
    static JsonObject getDctModified(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * adms:sample
     * Range: dcat:Distribution
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getAdmsSample(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:source
     * Range: dcat:Dataset
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getDctSource(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:spatial
     * Range: dct:Location
     * Used in: Dataset, Catalogue
     *
     * @param resource
     * @return
     */
    static JsonObject getDctSpatial(Resource resource) throws PropertyNotAvailableException {
        JsonObject result = new JsonObject();
        Resource spatialResource = PropertyHelper.getPropertyAsResource(resource, DCATAP.dctSpatial);
        Resource spatial = PropertyHelper.getResourceFromVocabulary(spatialResource);
        if(spatial == null) {
            throw new PropertyNotAvailableException("Spatial not in vocabulary.");
        }
        result.put("id", PropertyHelper.getSingleLiteral(spatial, EUVOC.dcIdentifier));

        StmtIterator stmtIterator = PropertyHelper.getPropertiesAsStmtIterator(spatial, EUVOC.skosPrefLabel);
        while(stmtIterator.hasNext()) {
            Statement stmt = stmtIterator.nextStatement();
            Literal literal = stmt.getLiteral();
            if(literal.getLanguage().equals("en")) {
                result.put("title", literal.getString());
            }
        }
        return result;
    }

    /**
     * dct:temporal
     * Range: dct:PeriodOfTime
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getDctTemporal(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:type
     * Range: skos:Concept
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getDctType(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * owl:versionInfo
     * Range: rdfs:Literal
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getOwlVersionInfo(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * adms:versionNotes
     * Range: rdfs:Literal
     * Used in: Dataset
     *
     * @param resource
     * @return
     */
    static JsonObject getAdmsVersionNotes(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dcat:accessURL
     * Range: rdfs:Resource
     * Used in: Distribution
     *
     * @param resource
     * @return
     */
    static String getDcatAccessUrl(Resource resource) throws PropertyNotAvailableException {
        Resource res = PropertyHelper.getPropertyAsResource(resource, DCATAP.dcatAccessURL);
        return res.getURI();
    }

    /**
     * dct:format
     * Range: dct:MediaTypeOrExtent
     * Used in: Distribution
     *
     * @param resource
     * @return
     */
    static String getDctFormat(Resource resource) throws PropertyNotAvailableException {
        Statement stmt = resource.getProperty(DCATAP.dctFormat);

        try {
            if(stmt != null) {
                RDFNode node = stmt.getObject();
                if(node.isResource()) {
                    Resource res = PropertyHelper.getResourceFromVocabulary(node.asResource());
                    if(res == null) {
                        try {
                            return PropertyHelper.getSingleLiteral(node.asResource(), RDFS.label);
                        } catch (PropertyNotAvailableException e) {
                            return PropertyHelper.extractLabelFromURI(node.asResource().getURI());
                        }
                    } else {
                        return PropertyHelper.getSingleLiteral(res, EUVOC.dcIdentifier);
                    }
                }
                if(node.isLiteral()) {
                    return node.asLiteral().getString();
                }
            } else {
                throw new PropertyNotAvailableException(DCATAP.dctFormat.toString() + " is not set");
            }
        } catch (RuntimeException e) {
            throw new PropertyNotAvailableException(e.getMessage());
        }

        return null;
    }

    /**
     * dct:license
     * Range: dct:LicenseDocument
     * Used in: Distribution, Catalogue
     *
     * @param resource
     * @return
     */
    static JsonObject getDctLicense(Resource resource) throws PropertyNotAvailableException {
        JsonObject result = new JsonObject();
        Statement stmt = resource.getProperty(DCATAP.dctLicense);
        try {
            if(stmt != null) {
                RDFNode node = stmt.getObject();
                if(node.isResource()) {
                    Resource res = PropertyHelper.getResourceFromVocabulary(node.asResource());
                    if(res == null) {

                        try {
                            result.put("id",  PropertyHelper.getSingleLiteral(node.asResource(), EUVOC.dcIdentifier));
                            result.put("title", PropertyHelper.getSingleLiteral(node.asResource(), EUVOC.skosAltLabel));
                            result.put("description", PropertyHelper.getSingleLiteral(node.asResource(), EUVOC.skosPrefLabel));
                            result.putNull("la_url");
                            result.put("resource", PropertyHelper.getPropertyAsResource(node.asResource(), EUVOC.skosExactMatch).getURI());
                        } catch (PropertyNotAvailableException e) {
                            String licence = PropertyHelper.extractLabelFromURI(node.asResource().getURI());
                            result.put("id", PropertyHelper.generateID(licence));
                            result.put("title", licence);
                            result.put("description", licence);
                            result.putNull("la_url");
                            result.put("resource", node.asResource().getURI());
                        }

                    } else {

                        Statement stmtSameAs = res.getProperty(OWL.sameAs);
                        if(stmtSameAs != null) {
                            res = PropertyHelper.getResourceFromVocabulary(stmtSameAs.getResource());
                        }

                        String id = PropertyHelper.getSingleLiteral(res, EUVOC.dcIdentifier);
                        String title = PropertyHelper.getEnglishLiteral(res, EUVOC.skosAltLabel);
                        String description = PropertyHelper.getEnglishLiteral(res, EUVOC.skosPrefLabel);
                        try {
                            Resource url  = PropertyHelper.getPropertyAsResource(res, EUVOC.skosExactMatch);
                            result.put("resource", url.getURI());
                        } catch (PropertyNotAvailableException e) {
                            result.putNull("resource");
                        }

                        try {
                            Resource resLA = PropertyHelper.getPropertyAsResource(res, EDP.edpLicensingAssistant);
                            result.put("la_url", resLA.getURI());
                        } catch (PropertyNotAvailableException e) {
                            result.putNull("in_la");
                        }

                        result.put("id", id);
                        if(title != null) {
                            result.put("title", title);
                        } else  {
                            result.put("title", id);
                        }

                        if(description != null) {
                            result.put("description", description);
                        } else  {
                            result.put("description", id);
                        }

                    }

                }
                if(node.isLiteral()) {
                    result.put("id", PropertyHelper.generateID(node.toString()));
                    result.put("title", node.toString());
                    result.put("description", node.toString());
                    result.putNull("la_url");
                    result.put("resource", false);
                }
            }

        } catch (RuntimeException e) {
            throw new PropertyNotAvailableException(e.getMessage());
        }

        if(result.isEmpty()) {
            throw new PropertyNotAvailableException("No valid " + DCATAP.dctLicense.toString() + " is set");
        }

        return result;
    }

    /**
     * dcat:byteSize
     * Range: rdfs:Literal (xsd:decimal)
     * Used in: Distribution
     *
     * @param resource
     * @return
     */
    static JsonObject getDcatByteSize(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * spdx:checksum
     * Range: spdx:Checksum
     * Used in: Distribution
     *
     * @param resource
     * @return
     */
    static JsonObject getSpdxChecksum(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dcat:downloadURL
     * Range: rdfs:Resource
     * Used in: Distribution
     *
     * @param resource
     * @return
     */
    static JsonObject getDcatDownloadUrl(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dcat:mediaType
     * Range: dct:MediaTypeOrExtent
     * Used in: Distribution
     *
     * @param resource
     * @return
     */
    static JsonObject getDcatMediaType(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:rights
     * Range: dct:RightsStatement
     * Used in: Distribution, Catalogue
     *
     * @param resource
     * @return
     */
    static JsonObject getDctRights(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * adms:status
     * Range: skos:Concept
     * Used in: Distribution
     *
     * Todo Unclear how to parse this property
     * @param resource
     * @return
     */
    static JsonObject getAdmsStatus(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * foaf:homepage
     * Range: foaf:Document
     * Used in: Catalogue
     *
     * @param resource
     * @return
     */
    static JsonObject getFoafHomepage(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dcat:ThemeTaxonomy
     * Range: skos:ConceptTheme
     * Used in: Catalogue
     *
     * @param resource
     * @return
     */
    static JsonObject getDcatThemeTaxonomy(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:hasPart
     * Range: dcat:Catalog
     * Used in: Catalogue
     *
     * @param resource
     * @return
     */
    static JsonObject getDctHasPart(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

    /**
     * dct:isPartOf
     * Range: dcat:Catalog
     * Used in: Catalogue
     *
     * @param resource
     * @return
     */
    static JsonObject getDctIsPartOf(Resource resource) throws PropertyNotAvailableException {
        throw new PropertyNotAvailableException();
    }

}
