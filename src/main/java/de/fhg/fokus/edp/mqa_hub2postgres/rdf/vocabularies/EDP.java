package de.fhg.fokus.edp.mqa_hub2postgres.rdf.vocabularies;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;

import java.util.HashMap;
import java.util.Map;

/**
 * Jena Classes and Resources for managing EDP
 * This is partly a dublicate from Jena build-in classes, but on purpose!
 * @Todo be completed
 *
 */
public class EDP {

    private static final Model m = ModelFactory.createDefaultModel();

    public static final String EDP_NS = "https://europeandataportal.eu/voc#";

    public static Map<String, String> getNsMap() {
        Map<String, String> map = new HashMap<>();
        map.put("edp", EDP_NS);
        return map;
    }

    //Classes

    //Properties
    public static final Property edpLicensingAssistant = m.createProperty(EDP_NS + "licensingAssistant");


}
