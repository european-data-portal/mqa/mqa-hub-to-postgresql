package de.fhg.fokus.edp.mqa_hub2postgres.rdf.vocabularies;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;

import java.util.HashMap;
import java.util.Map;

/**
 * Jena Classes and Resources for managing EuroVoc
 * This is partly a dublicate from Jena build-in classes, but on purpose!
 * @Todo be completed
 *
 */
public class EUVOC {

    private static final Model m = ModelFactory.createDefaultModel();

    public static final String AT_NS = "http://publications.europa.eu/ontology/authority/";
    public static final String DC_NS = "http://purl.org/dc/elements/1.1/";
    public static final String SKOS_NS = "http://www.w3.org/2004/02/skos/core#";

    public static Map<String, String> getNsMap() {
        Map<String, String> map = new HashMap<>();
        map.put("at", AT_NS);
        map.put("dc", DC_NS);
        map.put("skos", SKOS_NS);
        return map;
    }

    //Classes

    //Properties
    public static final Property atOpMappedCode = m.createProperty(AT_NS + "op-mapped-code");
    public static final Property atLegacyCode = m.createProperty(AT_NS + "legacy-code");
    public static final Property dcSource = m.createProperty(DC_NS + "source");
    public static final Property dcIdentifier = m.createProperty(DC_NS + "identifier");
    public static final Property skosPrefLabel = m.createProperty(SKOS_NS + "prefLabel");
    public static final Property skosAltLabel = m.createProperty(SKOS_NS + "altLabel");
    public static final Property skosExactMatch = m.createProperty(SKOS_NS + "exactMatch");

}
